/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nipitpon.ox_oop;

/**
 *
 * @author Wongsathorn Skd
 */
public class Table {

    private char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    private int lastrow;
    private int lastcol;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int column = 0; column < table[row].length; column++) {
                System.out.print(table[row][column]);
            }
            System.out.println("");
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkX() {
        if (table[0][0] == currentPlayer.getName()) {
            if (table[1][1] == currentPlayer.getName()) {
                if (table[2][2] == currentPlayer.getName()) {
                    finish = true;
                    winner = currentPlayer;
                    setStatWinLose();
                }
            }
        } else if (table[0][2] == currentPlayer.getName()) {
            if (table[1][1] == currentPlayer.getName()) {
                if (table[2][0] == currentPlayer.getName()) {
                    finish = true;
                    winner = currentPlayer;
                    setStatWinLose();

                }
            }
        }

    }

    void checkDraw() {
        //if (count == 9 && winner != 'x' && winner != 'o') {
        //  finish = true;
        //playerO.draw();
        //playerX.draw();

    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();
    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }

}
